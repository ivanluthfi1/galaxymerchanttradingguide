# Getting Started

### Guides
The following guides illustrates how to test this application:

I. By JUNIT FILE

1. open file src/test/java
2. right click on every single scripts
3. debug / run it


II. By POSTMAN
1. write down the url (see the controller) to the url adress in postman
2. because it is use GET method, so set the GET mehod in Postman
3. click SEND

Notes: the input file is "file.txt", so put the file wherever you want. Then write the file path in the parameter filePath when yout test this application.


Author: Ivan L. I.