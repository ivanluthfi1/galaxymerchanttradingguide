package com.application.logic;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ConvertionRulesTest {

	protected Character[] characterArray;
	protected Character character;

	@Before
	public void setUp() throws Exception {
		character = 'P';
		characterArray = new Character[]{'I','X','V','L'};

	}

	@Test
	public void testOutputFormatter(){
		boolean result = ConvertionRules.checkIfLiteralPresent(characterArray, character);
		Assert.assertEquals(false, result);
	}

	@Test
	public void testSubtractionLogic(){
		float result = ConvertionRules.subtractionLogic(52f, 10f, 50f);
		Assert.assertEquals(42f, result, 00.00);
	}

}
