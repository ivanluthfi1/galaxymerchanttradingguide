package com.application.controller;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.application.logic.InputProcessor;
import com.application.logic.OutputProcessor;


@RestController
public class ApplicationController {
	
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	protected String filePath;
	
	public void setUpStreams() {
		filePath = null;
		System.setOut(new PrintStream(outContent));
		System.setErr(new PrintStream(errContent));
	}
	
	
	public void cleanUpStreams() {
		System.setOut(null);
		System.setErr(null);
	}
	
	@RequestMapping(value="application/run", method = RequestMethod.GET)
	public ResponseEntity<?> RunTheApplication(@RequestParam(value="runTheCode") Integer runTheCode) {
		
		//if runTheCode = 1 the code will run else ==> Bad Request
		String Content = "no out content";
		try {
			if(runTheCode == 1) {
				filePath="E://file.txt";
				InputProcessor.ProcessFile(filePath);
				InputProcessor.MapTokentoIntegerValue();
				OutputProcessor.processReplyForQuestion();
				
				Content = outContent.toString();
			}
			else {
				return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
			}
			
		}catch(Exception e) {	
			
			e.printStackTrace();
			
		}
		return new ResponseEntity<>(Content, HttpStatus.OK);
	}

}
